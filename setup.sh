#!/bin/bash

DOT_PATH=$(pwd)


## vim
ln -sfn "$DOT_PATH/vim/vimrc" ~/.vimrc
ln -sfn "$DOT_PATH/vim" ~/.vim
ln -sfn "$DOT_PATH/tmux.conf" ~/.tmux.conf

mkdir -p "$DOT_PATH/vim/.swp/" "$DOT_PATH/vim/.backup" "$DOT_PATH/vim/.undo" 2>&1 > /dev/null

## zsh

if ! zsh_loc="$(type -p "zsh")" || [[ -z $zsh_loc ]]; then
	if [[ "$OSTYPE" == "linux-gnu" ]]; then
		sudo apt-get install zsh
	elif [[ "$OSTYPE" == "darwin"* ]]; then
		brew install zsh
	fi
	chsh -s $(which zsh)
	sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended
fi

ln -sfn "$DOT_PATH/zsh/zshrc" ~/.zshrc
ln -sfn "$DOT_PATH/zsh/zsh_custom" ~/.zsh_custom

git submodule sync
git submodule init
git submodule update

# PyEnv
#git clone https://github.com/pyenv/pyenv.git ~/.pyenv
#source ~/.zshrc
#git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv

# NVM
export NVM_DIR="$HOME/.nvm" && (
  git clone https://github.com/nvm-sh/nvm.git "$NVM_DIR"
  cd "$NVM_DIR"
  git checkout `git describe --abbrev=0 --tags --match "v[0-9]*" $(git rev-list --tags --max-count=1)`
) && \. "$NVM_DIR/nvm.sh"

brew install pyenv
brew install openssl readline sqlite3 xz zlib
brew install pyenv-virtualenv
