#!/bin/zsh

gh() { cd ~/github/$1 ;}
_gh()  {
    _files -/ -W ~/github
}
compdef _gh gh

gl() { cd ~/gitlab/$1 ;}
_gl()  {
    _files -/ -W ~/gitlab
}
compdef _gl gl

gitauth() { git config user.name "Max Thyron" ; git config user.email mkthyron@gmail.com ;}
bgitauth() { git config user.name "Karsakov Maxim" ; git config user.email mkthyron@gmail.com ;}
