set undofile
set undodir=~/.vim/.undo//
set backupdir=~/.vim/.backup//
set directory=~/.vim/.swp//

"" General
set number	" Show line numbers
set showmatch	" Highlight matching brace

set hlsearch	" Highlight all search results
set smartcase	" Enable smart-case search
set ignorecase	" Always case-insensitive
set incsearch	" Searches for strings incrementally

set autoindent	" Auto-indent new lines
set cindent	" Use 'C' style program indenting
set expandtab	" Use spaces instead of tabs
set shiftwidth=4	" Number of auto-indent spaces
set smartindent	" Enable smart-indent
set smarttab	" Enable smart-tabs
set softtabstop=4	" Number of spaces per Tab

"" Advanced
set ruler	" Show row and column ruler information
set mouse=a

set undolevels=3000	" Number of undo levels
set backspace=indent,eol,start	" Backspace behaviour

" if &term =~ 'st-256color'
"     set t_Co=16
" endif

execute pathogen#infect()

colorscheme murphy

"" Ruler
highlight ColorColumn ctermbg=black
call matchadd('ColorColumn', '\%81v', 100)

syntax on
filetype plugin indent on

nnoremap <silent> <F7> :nohlsearch<Bar>:echo<CR>

"" Better-whitespace
function! TidyWhitespace()
  if !&binary && &filetype != 'diff'
    normal mz
    normal Hmy
    %s/\s\+$//e
    normal 'yz<CR>
    normal `z
  endif
endfunction

"" Select pasted text
nnoremap gV `[V`]


"" Mapping

"" Remap F1
:nmap <F1> :echo<CR>
:imap <F1> <C-o>:echo<CR>

"" Russian Keymap
set langmap=ФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯЖ.;ABCDEFGHIJKLMNOPQRSTUVWXYZ:/,фисвуапршолдьтщзйкыегмцчня;abcdefghijklmnopqrstuvwxyz
